﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DarkLife
{
    public partial class FormMain : Form
    {
        EncryptValue soul = new EncryptValue( 0 );
        EncryptValue add = new EncryptValue(300);
        //SoulAction sap = new SoulAction();

        public FormMain()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            soul.value += add.value;
            tbSoul.Text = soul.value.ToString();
            soul = new EncryptValue(soul);
        }

        class EncryptValue
        {
            static System.Random random = new System.Random(System.Environment.TickCount);
            public EncryptValue(int a)
            {
                key = random.Next();
                _value = a ^ key; ;
            }
            public EncryptValue(EncryptValue a)
            {
                key = a.key;
                _value = a._value;
            }
            int key;
            int _value;
            public int value
            {
                get { return _value ^ key; }
                set { _value =value ^ key; }
            }
        }

        public class SoulAction
        {
            public DateTime time = DateTime.UtcNow;
            public uint action = 300;
            public uint currnet = 0;
        }

    }
}
